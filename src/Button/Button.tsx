import React from 'react';

import './button.css';

interface Props {
  children: React.ReactNode;
}

const Button: React.SFC<Props> = ({ children }) => (
  <div className="Button">
    {children}
  </div>
);

export default Button;
