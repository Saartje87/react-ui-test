process.env.BABEL_ENV = 'test';

module.exports = wallaby => ({
	files: [
		'tsconfig.json',
		'__mocks__/*.ts',
		'src/setupTests.ts',
		'src/**/*.ts?(x)',
		'src/**/*.snap',
		'!src/**/*.spec.ts?(x)',
	],
	tests: [
		'src/**/*.spec.ts?(x)',
	],
	env: {
		type: 'node',
		runner: 'node',
  },
  preprocessors: {
    '**/*.js?(x)': file => require('@babel/core')
      .transform(file.content, {
        sourceMaps: true,
        plugins: ['@babel/transform-modules-commonjs'],
      }),
  },
	testFramework: 'jest',
  // debug: true,
});
