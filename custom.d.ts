declare module '*.svg';
declare module '*.png';
declare module '*.jpg';
declare module '*.gif';

// `Global` version set by webpack
declare var __VERSION__: string;
